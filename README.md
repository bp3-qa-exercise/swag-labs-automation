# BP3 Global - QA Automation Exercise

## Overview

This readme provides instructions and information about the QA coding exercise focused on evaluating the Sauce Demo
website.

### Stack

- **Java**
- **Selenium**
- **Cucumber**
- **Extentreports**

### Usage

1. **Clone the Repository:**

   `git clone git@gitlab.com:bp3-qa-exercise/swag-labs-automation.git`


2. **Install Dependencies:**

   `mvn clean install`

3. **Execute Tests:**

  ```
  mvn test
  ```

4. **Review Test Report:**
   After test execution is complete, navigate to the `test-output/Test Report/SWAGLabs.html` directory to access the
   generated Extentreports HTML file. Open the relevant report in a web browser to view detailed test execution results,
   including passed tests, failed tests, and any accompanying error messages.