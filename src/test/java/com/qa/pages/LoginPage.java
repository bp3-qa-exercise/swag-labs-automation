package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.qa.utilities.Common.*;

public class LoginPage {
	private final WebDriver driver;

	@FindBy(className = "login_logo")
	private WebElement titleSwagLabs;

	@FindBy(id = "user-name")
	private WebElement txtUserName;

	@FindBy(id = "password")
	private WebElement txtPassword;

	@FindBy(id = "login-button")
	private WebElement btnLoginButton;

	@FindBy(css = "[data-test='error']")
	private WebElement errorMessage;

	@FindBy(className = "title")
	private WebElement titleProducts;

	@FindBy(xpath = "//button[@id='react-burger-menu-btn']")
	private WebElement menuBtn;

	@FindBy(xpath = "//a[@id='logout_sidebar_link']")
	private WebElement logoutBtn;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public Boolean isHeaderDisplayed() {
		waitForElementVisibility(driver, titleSwagLabs, 10);
		return titleSwagLabs.isDisplayed();
	}
	public void enterUsernameAndPassword(String username, String password) {
		txtUserName.sendKeys(username);
		txtPassword.sendKeys(password);
	}

	public void clickLoginButton() {
		btnLoginButton.click();
	}

	public String getSadFaceErrorMessage() {
		return errorMessage.getText();
	}

	public String getTitleProducts() {
		return titleProducts.getText();
	}
}
