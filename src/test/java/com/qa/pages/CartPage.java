package com.qa.pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import static org.testng.Assert.*;

public class CartPage {
	private final WebDriver driver;
	@FindBy(id = "item_4_title_link")
	private WebElement labelSauceLabsBackpack;

	@FindBy(id = "remove-sauce-labs-backpack")
	private WebElement btnRemoveFromCartBackpack;

	@FindBy(css = ".cart_item")
	private List<WebElement> cartItems;
	
	public CartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void isItemAddedToCartVisible(){
		String elementText = labelSauceLabsBackpack.getText();
		assertEquals("Sauce Labs Backpack", elementText);
		assertTrue(labelSauceLabsBackpack.isDisplayed());
	}

	public void removeItemFromCart(){
		btnRemoveFromCartBackpack.click();
	}

	public boolean isCartEmpty() {
		try {
			return cartItems.size() == 0;
		} catch (NoSuchElementException e) {
			System.out.println("No cart items found: " + e.getMessage());
			return true;
		}
	}
}
