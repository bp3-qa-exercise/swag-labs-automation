package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class FooterPage {
    private final WebDriver driver;
    @FindBy(css = "a[href*='twitter.com']")
    private WebElement linkTwitter;

    @FindBy(css = "a[href*='facebook.com']")
    private WebElement linkFacebook;

    @FindBy(css = "a[href*='linkedin.com']")
    private WebElement linkLinkedIn;

    public FooterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOnTwitter(){
        linkTwitter.click();
    }

    public void clickOnFacebook(){
        linkFacebook.click();
    }

    public void clickOnLinkedIn(){
        linkLinkedIn.click();
    }

    public void switchToTab(String tabHandle){
        driver.switchTo().window(tabHandle);
    }

    public List<String> getTabHandles(){
       return new ArrayList<>(driver.getWindowHandles());
    }

    public String getNewTabUrl(){
        return driver.getCurrentUrl();
    }
}
