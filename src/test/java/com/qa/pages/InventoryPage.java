package com.qa.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InventoryPage {
    private final WebDriver driver;
    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    private WebElement btnAddToCartBackpack;

    @FindBy(id = "add-to-cart-sauce-labs-bike-light")
    private WebElement btnAddToCartBikeLight;

    @FindBy(css = ".shopping_cart_badge")
    private WebElement iconCartBadge;

    @FindBy(className = "shopping_cart_link")
    private WebElement iconShoppingCartLink;

    public InventoryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void addToCart() {
        btnAddToCartBackpack.click();
    }

    public void addTwoItemsToCart(){
        btnAddToCartBackpack.click();
        btnAddToCartBikeLight.click();
    }
    public void navigateToShoppingCart() {
        iconShoppingCartLink.click();
    }

    public String getCartBadgeText() {
        return iconCartBadge.getText();
    }
}
