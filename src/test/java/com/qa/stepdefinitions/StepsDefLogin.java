package com.qa.stepdefinitions;

import com.qa.pages.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertTrue;

public class StepsDefLogin {
    LoginPage loginPage;
    private WebDriver driver;

    public StepsDefLogin() {
        this.driver = Hooks.getDriver();
    }

    @Given("I am on the sauce demo login page")
    public void iAmOnTheSauceDemoLoginPage() {
        driver.get(Hooks.getBaseUrl());
    }

    @When("I enter {string} and {string}")
    public void iEnterUsernameAndPassword(String username, String password) {
        loginPage = new LoginPage(driver);
        assertTrue(loginPage.isHeaderDisplayed(), "Login failed: 'SWAG Labs' header is not displayed.");
        loginPage.enterUsernameAndPassword(username, password);
    }

    @And("I click on the login button")
    public void iClickOnTheLoginButton() {
        loginPage.clickLoginButton();
    }

    @Then("I should see the expected {string}")
    public void iSeeTheExpectedOutcome(String outcome) {
        assertTrue(switch (outcome) {
            case "Products" -> loginPage.getTitleProducts().contains(outcome);
            default -> loginPage.getSadFaceErrorMessage().contains(outcome);
        });
    }
}
