package com.qa.stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;

public class Hooks {
    @Getter
    private static WebDriver driver;
    private static final String CONFIG_FILE = "config.properties";
    @Getter
    private static String baseUrl;

    @Before
    public void setUp() {
        loadConfig();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    private void loadConfig() {
        Properties properties = new Properties();
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE)) {
            if (inputStream != null) {
                properties.load(inputStream);
                baseUrl = properties.getProperty("baseUrl");
            } else {
                throw new RuntimeException("Config file '" + CONFIG_FILE + "' not found.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
