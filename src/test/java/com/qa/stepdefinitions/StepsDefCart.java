package com.qa.stepdefinitions;

import com.qa.pages.CartPage;
import com.qa.pages.FooterPage;
import com.qa.pages.InventoryPage;
import com.qa.pages.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class StepsDefCart {
    CartPage cartPage;
    LoginPage loginPage;

    FooterPage footerPage;
    InventoryPage inventoryPage;
    private WebDriver driver;

    public StepsDefCart() {
        this.driver = Hooks.getDriver();
    }

    @Given("I am logged in on the products page with username {string} and password {string}")
    public void iAmLoggedInOnTheProductsPage(String username, String password) {
        driver.get(Hooks.getBaseUrl());
        loginPage = new LoginPage(driver);
        loginPage.enterUsernameAndPassword(username, password);
        loginPage.clickLoginButton();
        inventoryPage = new InventoryPage(driver);
    }

    @When("I add a product to the cart")
    public void iAddAProductToTheCart() {
        inventoryPage.addToCart();
    }

    @When("I add two products to the cart")
    public void iAddTwoItemsToCart() {
        inventoryPage.addTwoItemsToCart();
    }

    @When("I navigate to cart and remove the product from the cart")
    public void iRemoveAProductFromTheCart() {
        inventoryPage.navigateToShoppingCart();
        cartPage = new CartPage(driver);
        cartPage.isItemAddedToCartVisible();
        cartPage.removeItemFromCart();
    }

    @Then("the product should be removed from the cart")
    public void theProductShouldBeRemovedFromTheCart() {
        assertTrue(cartPage.isCartEmpty(), "The cart is not empty.");
    }

    @Then("the product should be added to the cart")
    public void theProductShouldBeAddedToTheCart() {
        assertEquals(inventoryPage.getCartBadgeText(), "1", "The cart icon does not display the correct number(1) of items.");
    }

    @Then("the cart icon should display the number of items added")
    public void theCartIconShouldDisplayTheNumberOfItemsAdded() {
        assertEquals(inventoryPage.getCartBadgeText(), "2", "The cart icon does not display the correct number(2) of items.");
    }

    @When("I launch links and validate links with url containing {}")
    public void iClickOnNewTabsAndVerifyUrl(String expUrl){
        footerPage = new FooterPage(driver);
        footerPage.clickOnTwitter();
        footerPage.clickOnFacebook();
        footerPage.clickOnLinkedIn();

        List<String> tabHandles = footerPage.getTabHandles();
        assertTrue(tabHandles.size() > 1);

        String firstWindow = driver.getWindowHandle();
        for(String tabHandle : tabHandles){
            if(!tabHandle.equals(firstWindow)){
                footerPage.switchToTab(tabHandle);
                break;
            }
        }

        String newUrl = footerPage.getNewTabUrl();
        assertTrue(newUrl.contains(expUrl));

        //
        footerPage.switchToTab(firstWindow);

    }





}
