Feature: SWAG Labs - Login Functionality

  @smoke @login
  Scenario Outline: Login with multiple users
    Given I am on the sauce demo login page
    When I enter "<username>" and "<password>"
    And I click on the login button
    Then I should see the expected "<outcome>"

    Examples:
      | username        | password     | outcome                                            |
      | locked_out_user | secret_sauce | Epic sadface: Sorry, this user has been locked out |
      | standard_user   | secret_sauce | Products                                           |
      | problem_user    | secret_sauce | Products                                           |
      | invalid_user    | wrong_pass   | Epic sadface: Username and password do not match   |