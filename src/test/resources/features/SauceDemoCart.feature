Feature: SWAG Labs - Shopping Cart

  @smoke @cart
  Scenario: User adds an item to the Cart
    Given I am logged in on the products page with username "standard_user" and password "secret_sauce"
    When I add a product to the cart
    Then the product should be added to the cart

  @smoke @cart
  Scenario: Cart Icon Updates with Item Changes
    Given I am logged in on the products page with username "standard_user" and password "secret_sauce"
    When I add two products to the cart
    Then the cart icon should display the number of items added

  @smoke @cart
  Scenario: User Removes an Item from the Cart
    Given I am logged in on the products page with username "standard_user" and password "secret_sauce"
    When I add a product to the cart
    And I navigate to cart and remove the product from the cart
    Then the product should be removed from the cart

  @smoke @cart
  Scenario Outline: Validate social links
    Given I am logged in on the products page with username "standard_user" and password "secret_sauce"
    When I launch links and validate links with url containing "<url>"

    Examples:
      | url          |
      | linkedin.com |
      | facebook.com |
      | twiiter.com  |


